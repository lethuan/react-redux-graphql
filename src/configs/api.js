const METHODS = {
  get: 'GET',
  post: 'POST'
}
const QUERIES = {
  user: {
    listUser: `query getUsers($page:Int!){
      User{
        all(query: {page:$page}){
          _id
          name
          phone
          role
          email
          active
        }
      }
    }`,
    show: `query getUser($userID: ID!){
      User{
        show(_id: $userID){
          _id
          name
          phone
          role
          email
          active
        }
      }
    }`
  },
  role: {
    all: `query getRoles($page: Int!){
      Role{
        all(query: {page: $page}){
          _id
          name
          permissions
        }
      }
    }`,
    show: `query getRole($roleID: ID!){
      Role{
        show(_id: $roleID){
          _id
          name
          permissions
        }
      }
    }`
  },
  permission: {
    all: `query getPermissions{
      Permission{
        all
      }
    }`
  }
}
const MUTATIONS = {
  user: {
    create: `mutation createUser($userCreate: CREATE_USER){
      User{
        create(body: $userCreate){
           _id
           name
           phone
           email
           role
        }
      }
    }`,
    update: `mutation updateUser($userID: ID!,$updateCreate: UPDATE_USER){
      User{
        update(_id: $userID,body: $updateCreate){
           _id
           name
           phone
           email
           role
        }
      }
    }`,
    changeStatus: `mutation changeStatusUser($userID: ID!){
       User{
         changeStatus(_id: $userID){
          active
         }
       }
    }`
  },
  role: {
    create: `mutation createRole($roleCreate: CREATE_ROLE){
      Role{
        create(body: $roleCreate){
          _id
          name
          permissions
        }
      }
    }`,
    update: `mutation updateRole($roleID: ID!,$roleUpdate: UPDATE_ROLE){
      Role{
        update(_id:$roleID,body: $roleUpdate){
          _id
          name
          permissions
        }
      }
    }`,
    destroy: `mutation destroyRole($roleID: ID){
      Role{
        destroy(_id: $roleID)
      }
    }`,
    changeStatus: `mutation changeStatusRole($roleID: ID!){
      Role{
        changeStatus(_id: $roleID){
          active
        }
      }
    }`
  },
  permission: {
    create: `mutation createPermission($createPermission: [String!]){
      Permission{
        create(permissions: $createPermission){
          id
        }
      }
    }`,
    destroy: `mutation destroyPermission($permissionID:String){
       Permission{
          destroy(id: $permissionID)
       }
    }`
  }
}
export default {
  methods: METHODS,
  user: {
    listUser: () => {
      return {
        query: QUERIES.user.listUser,
        method: METHODS.post
      }
    },
    show: () => {
      return {
        query: QUERIES.user.show,
        method: METHODS.post
      }
    },
    create: () => {
      return {
        mutation: MUTATIONS.user.create,
        method: METHODS.post
      }
    },
    update: () => {
      return {
        mutation: MUTATIONS.user.update,
        method: METHODS.post
      }
    },
    changeStatus: () => {
      return {
        mutation: MUTATIONS.user.changeStatus,
        method: METHODS.post
      }
    }
  },
  role: {
    all: () => {
      return {
        query: QUERIES.role.all,
        method: METHODS.post
      }
    },
    show: () => {
      return {
        query: QUERIES.role.show,
        method: METHODS.post
      }
    },
    create: () => {
      return {
        mutation: MUTATIONS.role.create,
        method: METHODS.post
      }
    },
    update: () => {
      return {
        mutation: MUTATIONS.role.update,
        method: METHODS.post
      }
    },
    destroy: () => {
      return {
        mutation: MUTATIONS.role.destroy,
        method: METHODS.post
      }
    },
    changeStatus: () => {
      return {
        mutation: MUTATIONS.role.changeStatus,
        method: METHODS.post
      }
    }
  },
  permission: {
    all: () => {
      return {
        query: QUERIES.permission.all,
        method: METHODS.post
      }
    },
    create: () => {
      return {
        query: MUTATIONS.permission.create,
        method: METHODS.post
      }
    },
    destroy: () => {
      return {
        query: MUTATIONS.permission.destroy,
        method: METHODS.post
      }
    }
  }
}
