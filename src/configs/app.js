export default {
  endpoint: 'https://dev-doc-api.zody.vn/graphql',
  // Screen size
  screens: {
    'xs-max': 480,
    'sm-min': 481,
    'sm-max': 767,
    'md-min': 768,
    'md-max': 991,
    'lg-min': 992,
    'lg-max': 1199,
    'xl-min': 1200
  },
  notification: {
    success: 'success',
    error: 'error',
    warning: 'warning',
    info: 'info'
  }
}
