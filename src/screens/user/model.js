import { routerRedux } from 'dva/router'
import { listUser, createUser, showUser, updateUser, changeStatusUser } from './service'
import { MessageConst, AppConst } from '../../configs/index'
import { RcNotification } from '../../components/index'

export default {
  namespace: 'user',
  state: {
    isLoaded: false,
    data: [],
    show: {},
    filter: {
      page: 0,
      limit: 0,
      total: 0
    }
  },
  subscriptions: {},
  effects: {
    * listUser({ payload }, { call, put }) {
      const data = yield call(listUser, payload)
      if (!data) {
        return RcNotification(MessageConst.ServerError, AppConst.notification.error)
      }
      const response = data.data
      if (!response) {
        return RcNotification(response.message, AppConst.notification.error)
      }
      yield put({
        type: 'updateState',
        payload: {
          data: response.User.all
        },
        isLoaded: true
      })
    },
    * create({ payload }, { call, put }) {
      const data = yield call(createUser, payload)
      if (!data) {
        return RcNotification(MessageConst.ServerError, AppConst.notification.error)
      }
      const response = data.data
      if (!response) {
        return RcNotification(response.message, AppConst.notification.error)
      }
      // yield put({
      //   type: 'updateState',
      //   payload: {
      //     data: data.unshift([response.User.create])
      //   }
      // })
      yield put(routerRedux.push('/user'))
    },
    * show({ payload }, { call, put }) {
      const data = yield call(showUser, payload)
      if (!data) {
        return RcNotification(MessageConst.ServerError, AppConst.notification.error)
      }
      const response = data.data
      if (!response) {
        return RcNotification(response.message, AppConst.notification.error)
      }
      yield put({
        type: 'updateState',
        payload: {
          show: { ...response.User.show }
        }
      })
    },
    * update({ payload }, { call, put }) {
      const data = yield call(updateUser, payload.variables)
      if (!data) {
        return RcNotification(MessageConst.ServerError, AppConst.notification.error)
      }
      const response = data.data
      if (!response) {
        return RcNotification(response.message, AppConst.notification.error)
      }
      yield put(routerRedux.push('/user'))
    },
    * changeStatus({ payload }, { call, put, select }) {
      const data = yield call(changeStatusUser, payload.variables)
      if (!data) {
        return RcNotification(MessageConst.ServerError, AppConst.notification.error)
      }
      const response = data.data
      if (!response) {
        return RcNotification(response.message, AppConst.notification.error)
      }
      const user = { ...yield select(state => state.user.show) }
      user.active = response.User.changeStatus.active
      yield put({
        type: 'updateState',
        payload: {
          show: { ...user }
        }
      })
    }
  },
  reducers: {
    updateState(state, action) {
      let filter = state.filter
      if (action.payload.filter) {
        filter = Object.assign(filter, action.payload.filter)
      }
      action.payload.filter = filter
      return {
        ...state,
        ...action.payload
      }
    }
  }
}
