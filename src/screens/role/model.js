import { routerRedux } from 'dva/router'
import { all, show, changeStatus, create, destroy, update, allPermission } from './service'
import { MessageConst, AppConst } from '../../configs/index'
import { RcNotification } from '../../components/index'

export default {
  namespace: 'role',
  state: {
    isLoaded: false,
    allPermission: [],
    all: [],
    show: {},
    filter: {
      page: 0,
      limit: 0,
      total: 0
    }
  },
  subscriptions: {},
  effects: {
    * all({ payload }, { call, put }) {
      const data = yield call(all, payload)
      if (!data) {
        return RcNotification(MessageConst.ServerError, AppConst.notification.error)
      }
      const response = data.data
      if (!response) {
        return RcNotification(response.message, AppConst.notification.error)
      }
      yield put({
        type: 'updateState',
        payload: {
          all: response.Role.all
        },
        isLoaded: true
      })
    },
    * show({ payload }, { call, put }) {
      const data = yield call(show, payload)
      if (!data) {
        return RcNotification(MessageConst.ServerError, AppConst.notification.error)
      }
      const response = data.data
      if (!response) {
        return RcNotification(response.message, AppConst.notification.error)
      }
      yield put({
        type: 'updateState',
        payload: {
          show: { ...response.Role.show }
        }
      })
    },
    * update({ payload }, { call, put }) {
      const data = yield call(update, payload)
      if (!data) {
        return RcNotification(MessageConst.ServerError, AppConst.notification.error)
      }
      const response = data.data
      if (!response) {
        return RcNotification(response.message, AppConst.notification.error)
      }
      yield put(routerRedux.push('/role'))
    },
    * create({ payload }, { call, put }) {
      const data = yield call(create, payload)
      if (!data) {
        return RcNotification(MessageConst.ServerError, AppConst.notification.error)
      }
      const response = data.data
      if (!response) {
        return RcNotification(response.message, AppConst.notification.error)
      }
      yield put(routerRedux.push('/role'))
    },
    * destroy({ payload }, { call, put, select }) {
      const data = yield call(destroy, payload.variables)
      if (!data) {
        return RcNotification(MessageConst.ServerError, AppConst.notification.error)
      }
      const response = data.data
      if (!response) {
        return RcNotification(response.message, AppConst.notification.error)
      }
      const roles = yield select(state => state.role.all)
      roles.splice(payload.index, 1)
      yield put({
        type: 'updateState',
        payload: {
          all: roles
        }
      })
    },
    * changeStatus({ payload }, { call, put }) {
      const data = yield call(changeStatus, payload)
      if (!data) {
        return RcNotification(MessageConst.ServerError, AppConst.notification.error)
      }
      const response = data.data
      if (!response) {
        return RcNotification(response.message, AppConst.notification.error)
      }
      yield put({
        type: 'updateState',
        payload: {
          show: {
            ...payload,
            active: response.Role.changeStatus
          }
        }
      })
    },
    * allPermission({ payload }, { call, put }) {
      const data = yield call(allPermission, payload)
      if (!data) {
        return RcNotification(MessageConst.ServerError, AppConst.notification.error)
      }
      const response = data.data
      if (!response) {
        return RcNotification(response.message, AppConst.notification.error)
      }
      yield put({
        type: 'updateState',
        payload: {
          allPermission: response.Permission.all
        }
      })
    }
  },
  reducers: {
    updateState(state, action) {
      let filter = state.filter
      if (action.payload.filter) {
        filter = Object.assign(filter, action.payload.filter)
      }
      action.payload.filter = filter
      return {
        ...state,
        ...action.payload
      }
    }
  }
}
