import dva from 'dva'
import 'antd/dist/antd.less'
import './assets/styles/app.less'
import createHistory from 'history/createBrowserHistory'
import createLoading from 'dva-loading'

// 1. Initialize
const app = dva(
  {
    history: createHistory(),
    ...createLoading({
      effects: true,
    }),
    onError(error) {
      console.log('APP ERROR', error.message)
    }
  }
)

// 2. Plugins
// app.use({})

// 3. Model
// app.model(require('./screens/user/all/model'))
// 4. Router
app.router(require('./router'))

// 5. Start
app.start('#root')
