import React from 'react';
import { Button } from 'antd'

const Example = () => {
  return (
    <div>
      <Button>button</Button>
    </div>
  );
};

Example.propTypes = {};

export default Example;
